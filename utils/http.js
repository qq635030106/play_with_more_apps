import apiList from './api'   //  引入apiList.js文件

const http = (url, method, data, header = wx.getStorageSync('token')) => {     //接收所需要的参数，如果不够还可以自己自定义参数
  let promise = new Promise(function (resolve, reject) {
    wx.request({
      url: url,
      data: data ? data : null,
      method: method,
      header: header ? { authorization: `Bearer ${wx.getStorageSync('token')}` } : { 'content-type': 'application/x-www-form-urlencoded' },
      success: function (res) {
        //接口调用成功
        resolve(res);    //根据业务需要resolve接口返回的json的数据
      },
      fail: function (res) {
        // fail调用接口失败
        reject(res);
      }
    }) 
  })
  
  return promise;  //注意，这里返回的是promise对象
}



//游戏导航列表
let getGameList = (data) => {
  return http(apiList.getGameList, 'get', data)
}
//附近大神
let getNearPlayer = (data) => {
  return http(apiList.getNearPlayer, 'get', data)
}

//新人报道
let getNewPerson = (data) => {
  return http(apiList.getNewPerson, 'get', data)
}
//搜索
let getSearch = (data) => {
  return http(apiList.getSearch, 'get', data)
}
//取消订单
let cancelOrder = (order_id, data) => {
  return http(`${apiList.cancelOrder}/${order_id}/action/cancel`, 'put', data)
}
//该游戏下大神列表
let getGameMaster = (game_id, data) => {
  return http(`${apiList.getGameMaster}/${game_id}/master`, 'get', data)
}
//游戏详情
let gameDetail = (game_id, data) => {
  return http(`${apiList.gameDetail}/${game_id}`, 'get', data)
}
//用户信息
let getUser = (member_name, data) => {
  return http(`${apiList.getUser}/${member_name}`, 'get', data)
}
//获取所有游戏
let allGame = (data) => {
  return http(`${apiList.allGame}`, 'get', data)
}
//订单详情
let getOrderDetail = (order_id, data) => {
  return http(`${apiList.getOrderDetail}/${order_id}`, 'get', data)
}
//立即支付
let payment = (order_id, data) => {
  return http(`${apiList.payment}/${order_id}/payment`, 'post', data)
}
//用户同意开始订单
let agreeStart = (order_id, data) => {
  return http(`${apiList.agreeStart}/${order_id}/action/agree_start`, 'put', data)
}
//用户拒绝开始订单
let rejectOrder = (order_id, data) => {
  return http(`${apiList.rejectOrder}/${order_id}/action/reject_start`, 'put', data)
}
//用户确认完成订单
let completeOrder = (order_id, data) => {
  return http(`${apiList.completeOrder}/${order_id}/action/finish`, 'put', data)
}
//用户发起退款申请
let refund = (order_id, data) => {
  return http(`${apiList.refund}/${order_id}/action/apply_refund`, 'put', data)
}
//创建订单
let createOrder = (data) => {
  return http(`${apiList.createOrder}`, 'post', data)
}
//技能详情
let skillDetail = (skill_id, data) => {
  return http(`${apiList.skillDetail}/${skill_id}`, 'get', data)
}
//关注某个用户
let attentionUser = (user_id, data) => {
  return http(`${apiList.attentionUser}/${user_id}/follows`, 'post', data)
}
//获取某用户动态列表
let getPosts = (user_id, data) => {
  return http(`${apiList.getPosts}/${user_id}/posts`, 'get', data)
}
//我的关注
let follows = (data) => {
  return http(`${apiList.follows}`, 'get', data)
}
//我的粉丝
let fans = (data) => {
  return http(`${apiList.fans}`, 'get', data)
}
//编辑用户资料
let edit = (data) => {
  return http(`${apiList.edit}`, 'put', data)
}
//我的信息
let me = (data) => {
  return http(`${apiList.me}`, 'get', data)
}
//获取上传token
let getUploadToken = (data) => {
  return http(`${apiList.getUploadToken}`, 'post', data)
}
//发送验证码
let verificationCode = (data) => {
  return http(`${apiList.verificationCode}`, 'post', data)
}
//手机号注册用户或者登陆
let phoneLogin = (data) => {
  return http(`${apiList.phoneLogin}`, 'post', data)
}
//浏览足迹
let views = (data) => {
  return http(`${apiList.views}`, 'get', data)
}
//用户订单列表
let orders = (data) => {
  return http(`${apiList.orders}`, 'get', data)
}
//获取我的动态列表
let getMyPosts = (data) => {
  return http(`${apiList.getMyPosts}`, 'get', data)
}
//最近来访
let visitors = (data) => {
  return http(`${apiList.visitors}`, 'get', data)
}
//微信获取用户
let getWechat = (data) => {
  return http(`${apiList.getWechat}`, 'get', data)
}
//用户反馈
let feedback = (data) => {
  return http(`${apiList.getFeedback}`, 'post', data)
}


export default {
  getGameList, //游戏导航列表
  getNearPlayer, //附近大神
  getNewPerson, //新人报道
  getSearch, //搜索
  cancelOrder, //取消订单
  getGameMaster, //该游戏下大神列表
  gameDetail, //游戏详情
  getUser, //用户信息
  allGame, //获取所有游戏
  getOrderDetail, //订单详情
  payment, //立即支付
  agreeStart, //用户同意开始订单
  rejectOrder, //用户拒绝开始订单
  completeOrder, //用户确认完成订单
  refund, //用户发起退款申请
  createOrder, //创建订单
  skillDetail, //技能详情
  attentionUser, //关注某个用户
  getPosts, //获取某用户动态列表
  follows, //我的关注
  fans, //我的粉丝
  edit, //编辑用户资料
  me, //我的信息
  getUploadToken, //获取上传token
  verificationCode, //发送验证码
  phoneLogin, //手机号注册用户或者登陆
  views, //浏览足迹
  orders, //用户订单列表
  getMyPosts, //获取我的动态列表
  visitors, //最近来访
  getWechat, //微信获取用户
  feedback, //用户反馈
}
  