// let path = 'http://api.giggsgame.com/api'
let path = 'https://duoduo.apphw.com/api'

let apiList = {
  /* 首页 */
  getGameList: path + '/home/games',     //游戏导航列表
  getNearPlayer: path + '/masters',     //附近大神
  getNewPerson: path + '/masters',     // 新人报道
  getSearch: path + '/search',     // 搜索
  /* 订单 */
  cancelOrder: path + '/orders',     // 取消订单

  /* 游戏 */
  getGameMaster: path + '/games',     // 该游戏下大神列表
  gameDetail: path + '/games',     // 游戏详情
  allGame: path + '/games',     // 获取所有游戏

  /* 订单 */
  getOrderDetail: path + '/orders',     // 订单详情
  payment: path + '/orders',     // 立即支付
  agreeStart: path + '/orders',     // 用户同意开始订单
  rejectOrder: path + '/orders',     // 用户拒绝开始订单
  completeOrder: path + '/orders',     // 用户确认完成订单
  refund: path + '/orders',     // 用户发起退款申请
  createOrder: path + '/order',     // 创建订单
  orders: path + '/orders',     // 用户订单列表

  /* 技能 */
  skillDetail: path + '/master/games',     // 技能详情

  /* 用户 */
  getUser: path + '/users',     // 用户信息
  attentionUser: path + '/users',     // 关注某个用户
  follows: path + '/follows',     // 我的关注
  fans: path + '/fans',     // 我的粉丝
  edit: path + '/me',     // 编辑用户资料
  me: path + '/auth/me',     // 我的信息
  verificationCode: path + '/verification_codes',     // 发送验证码
  phoneLogin: path + '/users',     // 手机号注册用户或者登陆
  views: path + '/my/views',     // 浏览足迹
  visitors: path + '/my/visitors',     // 最近来访

  /* 动态 */
  getPosts: path + '/user',     // 获取某用户动态列表
  getMyPosts: path + '/user/posts',     // 获取我的动态列表

  /* 上传 */
  getUploadToken: path + '/auth/upload/token',     // 获取上传token
  getWechat: path + '/socials/wechat_service_account/authorizations',     // 微信获取用户
  getFeedback: path + '/feedback',     // 用户反馈


}

export default apiList