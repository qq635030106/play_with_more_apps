/* 游戏页 */
import api from '../../utils/http.js'
let app = getApp(),  pageStart = 1

Page({
  data: {
    player: [], //该游戏下的大神
    game_id: '', //当前游戏对应的id
    sex: '全部', //性别
    gender: '', //年龄
    allGrade: true, //全部段位
    price: '', //筛选中的价格
    priceList: [],
    grade: '', //筛选中的等级 
    gradeList: [],  //筛选中的段位
    allPrice: true, //全部价格
    aniStyle: true,  //下拉动画
    value1: 0,
    option1: [
      { text: '智能排序', value: 0 },
      { text: '最新大神', value: 1 },
    ],
    value2: 0,
    option2: [
      { text: '价格升序', value: 0 },
      { text: '价格降序', value: 1 },
    ],

    isIphoneX: app.globalData.isIphoneX,
    requesting: false,
    end: false,
    emptyShow: false,
    page: pageStart,
    hasTop: false,
    enableBackToTop: false,
    refreshSize: 90,
    bottomSize: 0,
    color: "#1ca0ff",
    lastPage: ''  //总页数
  },

  onLoad(options) {
    this.setData({game_id: options.game_id})
    
    // 该游戏下大神列表
    this.getGameMaster()

    // 游戏详情
    this.gameDetail()
  },

  // 该游戏下大神列表
  getGameMaster(){
    wx.showNavigationBarLoading()

    api.getGameMaster(this.data.game_id, { sort: this.data.value1, page: this.data.page}).then(res => {
      console.log('该游戏下大神列表', res.data.data)

      this.setData({ player: res.data.data, lastPage: res.data.meta.last_page })

      wx.hideNavigationBarLoading()
    })
  },

  onSwitch1Change({ detail }) {
    this.setData({
      value1: detail,
      page: 1,  // 初始化当前页数为第一页
    })
    // 该游戏下大神列表
    this.getGameMaster()
  },

  onSwitch2Change({detail}) {
    let player = this.data.player
    // 价格升序
    if (detail == 0) {
      for (var i = 0; i < player.length; i++) {
        for (var j = i + 1; j < player.length; j++) {
          if (player[i].price > player[j].price) {
            [player[i], player[j]] = [player[j], player[i]]
          }
        }
      }
      this.setData({player: player})

      // 价格降序
    } else if (detail == 1) {
      for (var i = 0; i < player.length; i++) {
        for (var j = i + 1; j < player.length; j++) {
          if (player[i].price < player[j].price) {
            [player[i], player[j]] = [player[j], player[i]]
          }
        }
      }
      this.setData({player: player})
    }
  },

  // 筛选性别
  changeSex(e) {
    this.setData({
      sex: `${e.currentTarget.dataset.sex}`,
      gender: `${e.currentTarget.dataset.gender}`,
    })
  },

  // 筛选段位
  gradeActive(e) {
    let gradeList = this.data.gradeList
    gradeList.forEach(item => {item.isSelected = false})
    this.setData({gradeList: gradeList})
    let index = e.currentTarget.id //当前点击元素索引
    let item = gradeList[index]
    item.isSelected = !item.isSelected
    this.setData({
      gradeList: gradeList,
      allGrade: false,
      grade: item.game_level_id
    })
  },

  // 全部段位
  allGradeActive() {
    let gradeList = this.data.gradeList
    gradeList.forEach(item => {item.isSelected = false})
    this.setData({
      allGrade: true,
      gradeList: gradeList,
      grade: ''
    })
  },

  // 筛选价格
  priceActive(e) {
    let priceList = this.data.priceList
    priceList.forEach(item => {item.isSelected = false})
    this.setData({priceList: priceList})
    let index = e.currentTarget.id //当前点击元素索引
    let item = priceList[index]
    item.isSelected = !item.isSelected
    this.setData({
      priceList: priceList,
      allPrice: false,
      price: item.text
    })
  },

  // 全部价格
  allPriceActive() {
    let priceList = this.data.priceList
    priceList.forEach(item => {item.isSelected = false})
    this.setData({
      allPrice: true,
      priceList: priceList,
      price: ''
    })
  },

  // 重置
  reset() {
    let { gradeList, priceList} = this.data
    gradeList.forEach(item => {item.isSelected = false})
    priceList.forEach(item => {item.isSelected = false})
    this.setData({
      allGrade: true,
      gradeList: gradeList,
      allPrice: true,
      priceList: priceList,
      sex: '全部',
      price: '',
      gender: '',
      grade: ''
    })
  },

  // 按条件筛选
  onConfirm(){
    this.selectComponent('#item').toggle(false);
    
    let { gender, price, grade, game_id } = this.data
    // 收集参数
    let params = { sort: this.data.value1, page: this.data.page, gender: gender, game_level_id: grade, price: price }

    // 该游戏下大神列表
    api.getGameMaster(game_id, params).then(res => {
      console.log('筛选后的数据', res.data.data)

      this.setData({ player: res.data.data, page: 1})
    })
  },

  // 游戏详情
  gameDetail(){
    api.gameDetail(this.data.game_id).then(res => {
      console.log('游戏详情', res.data.data)
      
      let result = res.data.data
      // 设置导航栏标题
      wx.setNavigationBarTitle({title: result.game_name})

      let gameLevel = result.level
      let priceList = result.price
      let arr = []
      gameLevel.forEach(item => { item.isSelected = false })
      priceList.forEach(item => { arr.push({text: item, isSelected: false}) })
      
      this.setData({ gradeList: gameLevel, priceList: arr })
    })
  },

  getList(type, currentPage) {
    this.setData({
      requesting: true
    })

    // 模拟异步获取数据场景
    setTimeout(() => {
      this.setData({
        requesting: false
      })

      if (type === 'refresh') {
        this.setData({
          page: currentPage,
          end: false
        })
        // 该游戏下大神列表
        this.getGameMaster()

      } else {
        // 请求到最后一页的数据了
        if (currentPage > this.data.lastPage) {
          this.setData({ end: true })
          return false
        }

        // 该游戏下大神列表
        api.getGameMaster(this.data.game_id, { sort: this.data.value1, page: currentPage }).then(res => {
          console.log('该游戏下大神列表', res.data.data)
          // push每一页的数据到数组中
          this.setData({
            player: [...this.data.player, ...res.data.data],
            page: currentPage
          })

        })
 
      }

    }, 1000);
  },
  // 下拉刷新
  refresh() {
    this.getList('refresh', pageStart);
  },
  // 上拉加载
  more() {
    this.getList('more', this.data.page + 1);
  }
})