/* 新人报道页 */
import api from '../../utils/http.js'
let app = getApp(),  pageStart = 1


Page({

  data: {
    newPerson1: [],
    isIphoneX: app.globalData.isIphoneX,
    requesting: false,
    end: false,
    emptyShow: false,
    page: pageStart,
    hasTop: false,
    enableBackToTop: false,
    refreshSize: 90,
    bottomSize: 0,
    color: "#1ca0ff",
    lastPage: ''  //总页数
  },

  onLoad() {
    // 新人报到
    this.getNewPerson()
  },

  // 新人报到
  getNewPerson(){
    wx.showNavigationBarLoading()

    api.getNewPerson({ type: 1, page_size: 18 }).then(res => {
      console.log('新人报到', res.data.data)

      this.setData({ newPerson1: res.data.data, lastPage: res.data.meta.last_page })

      wx.hideNavigationBarLoading()
    })
  },

  getList(type, currentPage) {
    this.setData({
      requesting: true
    })

    // 模拟异步获取数据场景
    setTimeout(() => {
      this.setData({
        requesting: false
      })

      if (type === 'refresh') {
        this.setData({
          page: currentPage,
          end: false
        })
        // 新人报到 
        this.getNewPerson()

      } else {
        // 请求到最后一页的数据了
        if (currentPage > this.data.lastPage) {
          this.setData({ end: true })
          return false
        }

        api.getNewPerson({ type: 1, page: currentPage, page_size: 6 }).then(res => {
          console.log('猜你喜欢', res.data.data)
          let result = res.data.data
          // push每一页的数据到数组中
          this.setData({ newPerson1: [...this.data.newPerson1, ...result], page: currentPage })
        })
      }

    }, 1000);
  },
  // 下拉刷新
  refresh() {
    this.getList('refresh', pageStart);
  },
  // 上拉加载
  more() {
    this.getList('more', this.data.page + 1);
  }
})