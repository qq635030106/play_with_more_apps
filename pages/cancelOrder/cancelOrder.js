/* 取消订单 */
import api from '../../utils/http.js'

Page({

  data: {
    order_id: '',
    words: 20, //字数限制
    inputValue: ''
  },

  onLoad(options) {
    this.setData({ order_id: options.order_id })
  },

  // 输入事件
  handleInput(e) {
    let {cursor, value} = e.detail
    this.setData({words: 20 - cursor, inputValue: value })
  },

  // 取消订单
  handleSubmit() { 
    api.cancelOrder(this.data.order_id, { describe: this.data.inputValue }).then(res => {
      if (res.statusCode === 200) {
        console.log('取消订单', res);
        
        wx.showToast({ title: '订单取消成功' })

        setTimeout(() => {wx.navigateBack({ delta: 1 })}, 1000)
      } 
    })
  },
}) 