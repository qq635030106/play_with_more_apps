/* 搜索页 */
import api from '../../utils/http.js'
let app = getApp(),  pageStart = 1

Page({

  data: {
    users: [],  //相关用户
    games: [],  //相关游戏
    searchFlag: false,  //是否显示历史搜索
    value: '', //input框的值
    key: '', //搜索关键字

    isIphoneX: app.globalData.isIphoneX,
    requesting: false,
    end: false,
    emptyShow: false,
    page: pageStart,
    hasTop: false,
    enableBackToTop: false,
    refreshSize: 90,
    bottomSize: 0,
    color: "#1ca0ff",
    lastPage: ''  //总页数
  },

  onLoad() {
    // 用户搜索过，就显示
    if (app.globalData.search.length) {
      this.setData({ searchHistory: app.globalData.search, searchFlag: true })
    }
  },

  // 搜索
  search(e) { 
    let key = e.currentTarget.dataset.key
    let value = e.detail.value
    // 如果内容为空给提示，不为空就搜索
    if (!value && !key) {
      wx.showToast({title: '输入不能为空',icon: 'none'})
    } else if (key) {
      this.searchFn(key)
    } else {
      this.searchFn(value)
    }

  },

  searchFn(value) {
    if(this.data.page != 1){
      this.setData({ page: 1})
    }
    wx.showNavigationBarLoading()
    // 搜索
    api.getSearch({ k: value, page: this.data.page}).then(res => {
      console.log('相关品类信息或者相关用户信息', res)
      
      // 搜索无数据
      if(!res.data.data.length && !res.data.games.length){
        this.setData({ users: [], games: [], searchFlag: false, key: value, emptyShow: true})
      }else{
        this.setData({ users: res.data.data, games: res.data.games, searchFlag: false, key: value, lastPage: res.data.meta.last_page})
      }
      
      wx.hideNavigationBarLoading()

    })
    // 将搜索的值push
    if (app.globalData.search.indexOf(value) == -1) {
      app.globalData.search.push(value)
    }

  },

  // 清空历史搜索
  delSearch(e) {
    let index = e.currentTarget.dataset.key
    app.globalData.search.splice(index, 1)

    if (app.globalData.search.length === 0){
      this.setData({ searchHistory: app.globalData.search, searchFlag: false })
    }else{
      this.setData({ searchHistory: app.globalData.search})
    }
  },

  // 清空所有历史搜索
  delAllSearch() {
    app.globalData.search = []
    this.setData({ searchFlag: false})
    
  },

  getList(type, currentPage) {
    this.setData({
      requesting: true
    })

    // 模拟异步获取数据场景
    setTimeout(() => {
      this.setData({
        requesting: false
      })

      if (type === 'refresh') {
        this.setData({
          page: currentPage,
          end: false
        })

      } else {
        // 请求到最后一页的数据了
        if (currentPage > this.data.lastPage) {
          this.setData({ end: true })
          return false
        }
        // 搜索
        api.getSearch({ k: this.data.key, page: currentPage }).then(res => {
          console.log('相关品类信息或者相关用户信息', res.data)
            this.setData({
              users: [...this.data.users, ...res.data.data],
              page: currentPage
            })

        })
      }

    }, 1000);
  },
  // 下拉刷新
  refresh() {
    this.getList('refresh', pageStart);
  },
  // 上拉加载
  more() {
    this.getList('more', this.data.page + 1);
  }
})