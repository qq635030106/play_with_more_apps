/* 确认订单页 */

import api from '../../utils/http.js'

let date = new Date()
let days = ['今天', '明天', '后天']
let hours = []
let minutes = ['00', '15', '30', '45']

let currentHour = date.getHours() //获取小时
if (currentHour.toString().length === 1) {
  currentHour = '0' + currentHour
}

let currentMinute = date.getMinutes() //获取分钟
if (currentMinute.toString().length === 1) {
  currentMinute = '0' + currentMinute
}

for (let i = 0; i <= 23; i++) {
  if (i.toString().length === 1) {
    i = '0' + i
  }
  hours.push(i)
}

Page({

  data: {
    days: days,
    day: '',
    hours: hours,
    hour: '',
    minutes: minutes,
    minute: '',
    value: [0, 0, 0],
    userInfo: {},
    modalFlag: false, //服务时间模态框
    number: 1, //数量
    remark: '',
    orders: {} //订单详情
  },

  onLoad(options) {
    // 获取上一个页面传来的数据
    const eventChannel = this.getOpenerEventChannel()
    eventChannel.on('sendUserInfoData', data => {
      console.log('技能资质页传来的数据', data.userInfo)
      this.setData({
        userInfo: data.userInfo
      })
    })
  },

  // 备注
  remark(e) {
    this.setData({remark: e.detail.value})
  },

  // 选择时间
  bindChange(e) {
    const val = e.detail.value
    // 设置当前时间
    this.setData({
      day: this.data.days[val[0]],
      hour: this.data.hours[val[1]],
      minute: this.data.minutes[val[2]]
    })
  },

  // 数量发生变化
  changePrice(e) {
    this.setData({number: e.detail})
  },

  // 提交订单
  toOrderDetails() {
    // 请先选择服务时间
    if (this.data.day === '') {
      wx.showToast({title: '请先选择服务时间',icon: 'none'})
      return false
    }

    // 收集参数
    let userInfo = this.data.userInfo
    let year = date.getFullYear()
    let month = date.getMonth() + 1
    let day = date.getDate()
    let second = date.getSeconds()
    let {
      hour,
      minute,
      number
    } = this.data
    if (month.toString().length === 1) month = '0' + month
    if (second.toString().length === 1) second = '0' + second

    switch (this.data.day) {
      case '今天':
        day = day
        break
      case '明天':
        day = day + 1
        break
      case '后天':
        day = day + 2
        break
    }
    if (day.toString().length === 1) day = '0' + day

    let params = {
      master_user_id: userInfo.user_id,
      game_id: userInfo.game_id,
      start_date: `${year}-${month}-${day} ${hour}:${minute}:${date.getSeconds()}`,
      num: number,
      unit: userInfo.unit,
      order_total: userInfo.price * this.data.number,
      remarks: this.data.remark
    }

    // 创建订单
    api.createOrder(params).then(res => {
      if (res.statusCode === 400) {
        wx.showToast({title: res.data.message,icon: 'none'})
      } else if (res.statusCode === 200) {
        console.log('创建订单', res)

        wx.showModal({
          content: '确定支付吗?',
          success: result => {
            if (result.confirm) {
              // 立即支付 
              api.payment(res.data.data.order_id, { type: 3 }).then(response => {
                if (res.statusCode === 200) {
                  console.log('立即支付', response)

                  // wx.showToast({title: response.data.message,icon: 'none'})

                  // 调用微信支付
                  // wx.requestPayment({
                  //     timeStamp : '', // 时间戳，必填（后台传回）
                  //     nonceStr : '', // 随机字符串，必填（后台传回）
                  //     package : '', // 统一下单接口返回的 prepay_id 参数值，必填（后台传回）
                  //     signType : 'MD5', // 签名算法，非必填，（预先约定或者后台传回）
                  //     paySign  : '', // 签名 ，必填 （后台传回）
                  //     success:function(res){ // 成功后的回调函数
                  //         // do something
                  //     }
                  // })
                }
              })
            } else if (result.cancel) {
              // 取消支付跳订单详情页
              wx.navigateTo({ url: `/pages/orderDetail/orderDetail?order_id=${res.data.data.order_id}` })
            }
          }
        })
      }
    })
  },

  // 服务时间模态框
  showServiceTime() {
    this.setData({modalFlag: !this.data.modalFlag})
  },

  // 保存时间
  saveTime() {
    this.setData({modalFlag: !this.data.modalFlag})
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },
})