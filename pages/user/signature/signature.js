/* 签名页 */

import api from '../../../utils/http.js'

Page({

  data: { 
    words: '20', //字数限制
    inputValue: '', //输入框的值
  },

  // 文字提示
  changeLength(e) {
    let wordsLen = e.detail.cursor //输入的值的长度
    let inputValue = e.detail.value //输入的值

    this.setData({
      words: 20 - wordsLen,
      inputValue: inputValue
    })
  }, 

  // 提交修改
  submit() {
    wx.showNavigationBarLoading()

    // 编辑用户资料
    api.edit({ sign: this.data.inputValue }).then(res => {
      console.log('编辑用户资料', res)

      wx.hideNavigationBarLoading()

      wx.showToast({ title: '修改成功', icon: 'none' })  //提示签名修改成功

      // 返回上一页
      setTimeout(() => { wx.navigateBack({ delta: 1 }) }, 500)
    })
  },
})