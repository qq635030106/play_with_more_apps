/* 关注页 */
import api from '../../../utils/http.js' 
let app = getApp(),  pageStart = 1

Page({ 

  data: { 
    followArr: [],  //我的关注
    isIphoneX: app.globalData.isIphoneX,
    requesting: false,
    end: false,
    emptyShow: false,
    page: pageStart,
    hasTop: false,
    enableBackToTop: false,
    refreshSize: 90,
    bottomSize: 0,
    color: "#1ca0ff",
    lastPage: ''  //总页数
  },

  onLoad() {
    // 我的关注
    this.getFollows()
  }, 

  // 我的关注
  getFollows(){
    api.follows({ page: this.data.page }).then(res => {
      console.log('我的关注', res.data.data)
      this.setData({ followArr: res.data.data, lastPage: res.data.meta.last_page })
      
    })
  },

  getList(type, currentPage) {
    this.setData({
      requesting: true
    })

    // 模拟异步获取数据场景
    setTimeout(() => {
      this.setData({
        requesting: false
      })

      if (type === 'refresh') {
        this.setData({
          page: currentPage,
          end: false
        })
        // 我的关注
        this.getFollows()

      } else {
        // 请求到最后一页的数据了
        if (currentPage > this.data.lastPage) {
          this.setData({ end: true })
          return false
        }

        api.follows({page: currentPage}).then(res => {
          console.log('我的关注', res.data.data)
          // push每一页的数据到数组中
          this.setData({ followArr: [...this.data.followArr, ...res.data.data], page: currentPage })
        })
      }

    }, 1000);
  },
  // 下拉刷新
  refresh() {
    this.getList('refresh', pageStart);
  },
  // 上拉加载
  more() {
    this.getList('more', this.data.page + 1);
  }
})