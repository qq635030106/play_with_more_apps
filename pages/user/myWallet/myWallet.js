/* 我的钱包页 */

import api from '../../../utils/http.js'

Page({

  data: {
    money: ''
  },

  onShow() {
    // 我的信息
    api.me().then(res => {
      console.log('我的信息', res.data.data)

      this.setData({ money: Number(res.data.data.currency.coin).toFixed(2) })
    })
  },

  // 跳转到充值中心页
  toPayPage(){
    wx.navigateTo({url: '/pages/user/pay/pay'})
  }
})