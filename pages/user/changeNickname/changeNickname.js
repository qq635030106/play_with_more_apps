/* 修改昵称页 */

import api from '../../../utils/http.js'

Page({
 
  data: {
    nickName: ''
  },

  // 输入时触发
  bindinput(e) {
    this.setData({nickName: e.detail.value})
  },

  // 清空输入
  cancelInput() {
    this.setData({nickName: ''})
  },

  // 修改昵称
  changeNickname() {
    if (this.data.nickName === '') {
      wx.showToast({title: '昵称不能为空',icon: 'none'})
    } else {
      wx.showLoading({title: '玩命加载中'})

      //编辑用户资料
      api.edit({ nickname: this.data.nickName }).then(res => {
        console.log('编辑用户资料', res)

        if (res.statusCode === 405) {
          wx.showToast({ title: '昵称必须介于 3 - 15 个字符之间', icon: 'none' })
        } else {
          wx.hideLoading()

          wx.showToast({ title: '修改成功' })

          wx.setStorage({ key: 'userInfo', data: res.data.data })

          setTimeout(() => {wx.navigateBack({ delta: 1 })}, 500)
        }
      })
    }
  },
})