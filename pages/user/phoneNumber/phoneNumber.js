Page({
  data: {
    phone: '',  //手机号码
    btnBright: true, //按钮可否点击
  },

  // 输入手机号时触发
  phoneValue(e) {
    if (e.detail.value.length === 11) {
      this.setData({btnBright: false, phone: e.detail.value})
    } else {
      this.setData({btnBright: true})
    }
  },

  // 下一步
  nextStep() {
    if (/^1[3456789]\d{9}$/.test(this.data.phone)) {
      wx.navigateTo({url: `/pages/user/codeLogin/codeLogin?phone=${this.data.phone}`})
      
    } else {
      wx.showToast({ title: '手机号码格式不正确',icon: 'none' })
    }
  },
})