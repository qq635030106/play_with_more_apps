/* 个人中心页 */
import api from '../../../utils/http'

Page({
  data: {
    userInfo: {},
    showDialog: false,
    dialogFlag: false,
    // 更多服务
    moreService: [{
        imgSrc: '/images/mine_wallet.png',
        text: '我的钱包',
        name: 'myWallet'
      },
      {
        imgSrc: '/images/mine_Order.png',
        text: '订单',
        name: 'order'
      },
      {
        imgSrc: '/images/mine_threw.png',
        text: '反馈',
        name: 'feedback'
      },
      {
        imgSrc: '/images/mine_customer.png',
        text: '联系客服',
        name: 'call'
      }
    ],
    showLogoutDialog: false,
    token: ''
  },

  onLoad(){
    this.setData({
      token: wx.getStorageSync('token'),
      dialogFlag: wx.getStorageSync('token') ? true : false
    })
  },

  onShow() {
    // 我的信息
    this.getMe()
  },

  logout(){
    { this.setData({showLogoutDialog: true}) }
  },
  /* 退出登录 */
  confirmLogout(){
    wx.clearStorage()
    setTimeout(() => {
      wx.reLaunch({url: '/pages/home/home'})
    }, 500)
  },
  
  // 我的信息
  getMe() {
    api.me().then(res => {
      console.log('我的信息', res.data)

      this.setData({ userInfo: res.data.data })
    })
  },

  // 去登录
  toLogin() { wx.navigateTo({url: '/pages/user/codeLogin/codeLogin'}) },

  // 跳转方法
  jump(e){
    if(!this.data.dialogFlag){
      this.setData({showDialog: true})
    }else{ 
      switch(e.currentTarget.dataset.name){
        case 'personalHomePage':
          wx.navigateTo({url: '/pages/user/personalHomePage/personalHomePage'})
          break
        case 'attention':
          wx.navigateTo({url: '/pages/user/attention/attention'})
          break
        case 'fans':
          wx.navigateTo({url: '/pages/user/fans/fans'})
          break
        case 'recent':
          wx.navigateTo({url: '/pages/user/recent/recent'})
          break
        case 'footprint':
          wx.navigateTo({url: '/pages/user/footprint/footprint'})
          break
        case 'myWallet':
          wx.navigateTo({url: '/pages/user/myWallet/myWallet'})
          break
        case 'order':
          wx.navigateTo({url: '/pages/user/order/order'})
          break
        case 'feedback':
          wx.navigateTo({url: '/pages/user/feedback/feedback'})
          break
        case 'call':
          wx.makePhoneCall({codeLogin: '028-83700200'})
          break
        default:
          wx.navigateTo({url: '/pages/user/codeLogin/codeLogin'})
          break
      }
    }
  }
})