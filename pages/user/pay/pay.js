/* 充值中心页 */

import api from '../../../utils/http.js'
 
Page({

  data: {
    moneyList: [{
        id: '1',
        imgSrc: '/images/money.png',
        money: '10',
        isSelected: false
      },
      {
        id: '2',
        imgSrc: '/images/money.png',
        money: '30',
        isSelected: false
      },
      {
        id: '3',
        imgSrc: '/images/money.png',
        money: '50',
        isSelected: false
      },
      {
        id: '4',
        imgSrc: '/images/money.png',
        money: '100',
        isSelected: false
      },
      {
        id: '5',
        imgSrc: '/images/money.png',
        money: '500',
        isSelected: false
      },
      {
        id: '6',
        imgSrc: '/images/money.png',
        money: '1000',
        isSelected: false
      }
    ],
    money: '',
    userInfo: {}
  },

  // 选择金额
  pitch(e) {
    let moneyList = this.data.moneyList
    let index = e.currentTarget.id
    let item = moneyList[index]
    if (!item.isSelected) moneyList.forEach(item => {item.isSelected = false})
    item.isSelected = !item.isSelected
    this.setData({
      moneyList: moneyList,
      money: ''
    })
  },

  // 输入充值金额
  inputMoney(e) {
    this.setData({money: e.detail.value})
  },

  // 立即充值
  pay() {
    let {moneyList,money} = this.data
    let result = moneyList.filter(item => item.isSelected === true)
    if (result.length === 0 && money === '') {
      wx.showToast({title: '请选择要充值的金额',icon: 'none'})
    } else {
      money = money.replace(/\b(0+)/gi, '')
      if (money === '' && result.length === 0) {
        wx.showToast({title: '参数不正确',icon: 'none'})
      } else {
        console.log('微信支付')
      }
    }
  },
  //输入框聚焦，取消所选金额
  bindfocus() {
    let moneyList = this.data.moneyList
    moneyList.forEach(item => {item.isSelected = false})
    this.setData({moneyList: moneyList})
  },

  onLoad(options) {
    // 我的信息
    api.me().then(res => {
      console.log('我的信息', res.data)

      this.setData({ userInfo: res.data.data })
    })
  },
})