import api from '../../../utils/http'

Page({

  data: { 
    value: ''
  },

  handleInput(e) {
    this.setData({
      value: e.detail.value
    })
  }, 

  submit() {
    wx.showNavigationBarLoading()

    // 用户反馈
    api.feedback({ content: this.data.value }).then(res => {
      console.log('用户反馈', res)

      wx.hideNavigationBarLoading()

      wx.showToast({ title: res.data.message})

      // 返回上一页
      setTimeout(() => { wx.navigateBack({ delta: 1 }) }, 500)
    })
  },
})