/* 出生日期页 */

import api from '../../../utils/http.js'

Page({

  data: {
    age: '', //年龄 
    zodiac: '', //星座
    minDate: new Date(1900, 0, 1).getTime(),  //最小日期
    maxDate: new Date().getTime(),  //最大日期
    show: false, //显示datePicker
    currentDate: new Date().getTime(),  //当前选中值
  },

  onLoad(options) {
    let _this = this
    let { year, month, day } = _this.data
    let { age, zodiac } = options
    // 设置年龄和星座
    _this.setData({ age: age, zodiac: zodiac })
  },

  // datePicker值发生改变
  onInput(event) {
    this.setData({currentDate: event.detail});
  },
  // 显示datePicker
  showPopup() {
    this.setData({ show: true });
  },
  // 隐藏datePicker
  onClose() {
    this.setData({ show: false });
  },

  // 修改年月日
  changeBirthday(value) {
    let date = new Date(value.detail)
    var myDate = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate()
    
    wx.showNavigationBarLoading()
    // 编辑用户资料
    api.edit({ birthday: myDate }).then(res => {
      console.log('编辑用户资料', res)

      this.onClose()

      wx.hideNavigationBarLoading()

      wx.showToast({ title: '修改成功' })

      wx.setStorage({ key: 'userInfo', data: res.data.data })

      // 修改成功自动返回上一页
      setTimeout(() => { wx.navigateBack({ delta: 1 }) }, 500)
    })
  },
})