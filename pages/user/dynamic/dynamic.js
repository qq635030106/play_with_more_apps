/* 全部动态页 */
import api from '../../../utils/http.js'
let app = getApp(), pageStart = 1

Page({

  data: {
    dynamicDetails: [], //全部动态
    dynamicId: '',  //动态详情id
    isIphoneX: app.globalData.isIphoneX,
    requesting: false,
    end: false,
    emptyShow: false,
    page: pageStart,
    hasTop: false,
    enableBackToTop: false,
    refreshSize: 90,
    bottomSize: 0,
    color: "#1ca0ff",
    lastPage: ''  //总页数
  },

  onLoad(options) { 
    // 获取我的动态详情
    this.getDynamic(options.id)
  },
 
  // 获取某用户动态列表
  getDynamic(id){
    api.getPosts(id, {page: this.data.page}).then(res => {
      console.log('获取某用户动态列表', res.data.data)

      this.setData({ dynamicDetails: res.data.data, dynamicId: id, lastPage: res.data.meta.last_page  })
    })
  },

  getList(type, currentPage) {
    this.setData({
      requesting: true
    })

    // 模拟异步获取数据场景
    setTimeout(() => {
      this.setData({
        requesting: false
      })

      if (type === 'refresh') {
        this.setData({
          page: currentPage,
          end: false
        })
        // 获取我的动态详情
        this.getDynamic(this.data.dynamicId)

      } else {
        // 请求到最后一页的数据了
        if (currentPage > this.data.lastPage) {
          this.setData({ end: true })
          return false
        }

        api.getPosts(this.data.dynamicId, { page: currentPage }).then(res => {
          console.log('获取某用户动态列表', res.data.data)
          // push每一页的数据到数组中
          this.setData({ dynamicDetails: [...this.data.dynamicDetails, ...res.data.data], page: currentPage })
        })
      }

    }, 1000);
  },
  // 下拉刷新
  refresh() {
    this.getList('refresh', pageStart);
  },
  // 上拉加载
  more() {
    this.getList('more', this.data.page + 1);
  }
})