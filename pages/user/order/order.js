/* 订单页 */
import api from '../../../utils/http.js'
let app = getApp(),  pageStart = 1

Page({ 

  data: { 
    now: [], //进行中的订单
    history: [], //历史订单 
    current: '0',

    isIphoneX: app.globalData.isIphoneX,
    requesting: false,
    end: false,
    emptyShow: false,
    page: pageStart,
    hasTop: false,
    enableBackToTop: false,
    refreshSize: 90,
    bottomSize: 0,
    color: "#1ca0ff",
    lastPage: ''  //总页数
  }, 

  onLoad() {
    // 进行中的订单
    this.getOrder('present')
  },

  // 进行中的订单
  getOrder(type =''){
    wx.showNavigationBarLoading()

    // 用户订单列表
    api.orders({type: type}).then(res => {
      console.log('用户订单列表', res.data.data)

      if (type === 'present') {
        this.setData({ now: res.data.data})
      } else {
        this.setData({ history: res.data.data, lastPage: res.data.meta.last_page })
      }

      wx.hideNavigationBarLoading()
    })
  },

  // 当前激活的标签改变时触发
  onChange(e) {
    this.setData({
      current: e.detail.key, 
      page: 1, 
    })

    if (e.detail.key === '0'){
      // 进行中的订单
      this.getOrder('present')

    } else if (e.detail.key === '1'){
      this.setData({ end: false})
      // 历史订单 
      this.getOrder()
    }
  },

  getList(type, currentPage) {
    this.setData({
      requesting: true
    })

    // 模拟异步获取数据场景
    setTimeout(() => {
      this.setData({
        requesting: false
      })

      if (type === 'refresh') {
        this.setData({
          page: currentPage,
          end: false
        })
        if (this.data.current === '0') {
          this.getOrder('present')
        } else {
          this.getOrder('')
        }

      } else {
        if(this.data.current === '0'){
          this.setData({ end: true })
          return false
        }
        // 请求到最后一页的数据了
        if (currentPage > this.data.lastPage) {
          this.setData({ end: true })
          return false
        }

        api.orders({ page: currentPage }).then(res => {
          console.log('历史订单', res.data.data)
          // push每一页的数据到数组中
          this.setData({ history: [...this.data.history, ...res.data.data], page: currentPage })
        })
      }

    }, 1000);
  },
  // 下拉刷新
  refresh() {
    this.getList('refresh', pageStart);
  },
  // 上拉加载
  more() {
    this.getList('more', this.data.page + 1);
  }
})