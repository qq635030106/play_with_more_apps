/* 编辑资料页 */
import api from '../../../utils/http.js'

Page({
  data: {
    userInfo: {}, //我的信息
    upload_token: '' //上传图片所需的token
  },
 
  onShow() {
    // 我的信息
    this.getMeInfo()
  },

  // 获取我的信息
  getMeInfo(){ 
    // 我的信息
    api.me().then(res => {
      console.log('我的信息', res.data)

      res.data.data.birthday = res.data.data.birthday.split(' ')[0] //截取出生日期年月日

      this.setData({ userInfo: res.data.data })
    })
  },

  // 修改头像
  changeAvatar() {
    // 获取上传token
    api.getUploadToken({ type: 'images' }).then(res => {
      console.log('获取上传token', res)

      this.setData({ upload_token: res.data.data.upload_token })
    })
  
    // 选择头像
    wx.chooseImage({
      count: 1, //最多可以选择的头像张数
      sizeType: ['compressed'], //所选的头像的尺寸
      sourceType: ['album', 'camera'],  //选择头像的来源
      success: res => {
        wx.showLoading({ title: '玩命加载中' })

        let { userInfo, upload_token} = this.data
        let tempFilePaths = res.tempFilePaths //头像路径
        let timestamp = new Date().getTime()  //时间戳
        let fileName = `app/image/${userInfo.id}/${timestamp}`  //文件名
        // 将本地资源上传到服务器
        wx.uploadFile({
          url: 'http://up-z2.qiniup.com',
          filePath: tempFilePaths[0], //要上传文件资源的路径
          name: 'file', //文件对应的key，开发者在服务端可以通过这个 key 获取文件的二进制内容
          formData: { //HTTP请求中其他额外的formdata
            token: upload_token,
            key: fileName
          },
          success: res => {
            //编辑用户资料
            api.edit({ token: wx.getStorageSync('token'), avatar: fileName }).then(res => {
              console.log('编辑用户资料', res)

              // 更新本地缓存的头像
              wx.setStorage({ key: 'myAvatar', data: res.data.data.avatar, })

              this.setData({ userInfo: res.data.data })

              wx.hideLoading()

              wx.showToast({ title: '头像修改成功' })
            })
          }
        })
      }
    })
  },
}) 