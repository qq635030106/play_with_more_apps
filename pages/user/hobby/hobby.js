/* 兴趣页 */
import api from '../../../utils/http.js'

Page({

  data: {
    // 兴趣数列表
    tagList: [{
        id: '1',
        text: '星座',
        isSelected: false,
      }, {
        id: '2',
        text: '炉石传说',
        isSelected: false,
      }, {
        id: '3',
        text: '密室逃脱',
        isSelected: false,
      },
      {
        id: '4',
        text: 'H1Z1',
        isSelected: false,
      }, {
        id: '5',
        text: '魔兽世界',
        isSelected: false,
      }, {
        id: '6',
        text: '饥荒',
        isSelected: false,
      },
      {
        id: '7',
        text: '占卜',
        isSelected: false,
      }, {
        id: '8',
        text: '美食',
        isSelected: false,
      }, {
        id: '9',
        text: 'DOTA2',
        isSelected: false,
      },
      {
        id: '10',
        text: '电影',
        isSelected: false,
      }, {
        id: '11',
        text: '旅游',
        isSelected: false,
      }, {
        id: '12',
        text: 'K歌',
        isSelected: false,
      },
      {
        id: '13',
        text: '健身',
        isSelected: false,
      }, {
        id: '14',
        text: '外语',
        isSelected: false,
      }, {
        id: '15',
        text: '守望先锋',
        isSelected: false,
      }, {
        id: '16',
        text: 'LOL',
        isSelected: false,
      },
      {
        id: '17',
        text: '签名',
        isSelected: false,
      }, {
        id: '18',
        text: '视频',
        isSelected: false,
      }, {
        id: '19',
        text: '英雄杀',
        isSelected: false,
      }, {
        id: '20',
        text: '桌游',
        isSelected: false
      },
      {
        id: '21',
        text: '声控',
        isSelected: false,
      }, {
        id: '22',
        text: '王者荣耀',
        isSelected: false,
      }, {
        id: '23',
        text: 'Cosplay',
        isSelected: false,
      },
      {
        id: '24',
        text: '陪打牌',
        isSelected: false,
      }, {
        id: '25',
        text: '叫醒',
        isSelected: false,
      }, {
        id: '26',
        text: '球球大作战',
        isSelected: false,
      }, {
        id: '27',
        text: '桌球',
        isSelected: false,
      },
      {
        id: '28',
        text: '心里咨询',
        isSelected: false,
      }, {
        id: '29',
        text: 'PS',
        isSelected: false,
      }, {
        id: '30',
        text: '时尚',
        isSelected: false,
      },
      {
        id: '31',
        text: '沙画',
        isSelected: false,
      }, {
        id: '32',
        text: '手绘',
        isSelected: false,
      }
    ],
    hobbyNum: 0, //兴趣数
    hobbyTagArr: [], //选中的兴趣数
  },

  onLoad(options) {
    let { tagList, hobbyTagArr } = this.data

    // 获取上一页传来的兴趣数,使其呈现选中状态
    tagList.forEach((el, index) => {
      let hobby = el.text  //兴趣数

      if (options.hobby.indexOf(hobby) != -1) {
        hobbyTagArr.push(hobby) //选中的兴趣数添加到新数组
        el.isSelected = !el.isSelected

        this.setData({
          tagList: tagList,
          hobbyNum: hobbyTagArr.length
        })
      }
    })
  },
  
  // 选择兴趣数
  selectHobby(e) {
    let { tagList, hobbyNum, hobbyTagArr} = this.data
    let index = e.currentTarget.id //当前点击元素索引
    let item = tagList[index]
    item.isSelected = !item.isSelected
    // 过滤出所有选中的兴趣数
    let result = tagList.filter(item => item.isSelected)

    // 兴趣数选中就加一,取消就减一
    if (item.isSelected && result.length <= 5) {
      this.setData({hobbyNum: hobbyNum + 1})

      // 将兴趣数添加到数组中
      hobbyTagArr.push(item.text)
    } else if (!item.isSelected) {
      this.setData({hobbyNum: hobbyNum - 1})

      hobbyTagArr.forEach((el, index) => {
        // 从数组中删除取消选中的兴趣数
        if (el == item.text) hobbyTagArr.splice(index, 1)
      })
    }

    // 兴趣数大于五给提示
    if (result.length > 5 && item.isSelected) {
      item.isSelected = !item.isSelected;
      this.setData({tagList: tagList})
      wx.showToast({title: '最多只可以选择五个兴趣数',icon: 'none'})
    } else {
      this.setData({tagList: tagList})
    }
  },

  // 确定修改
  submitBtn() {
    //编辑用户资料
    api.edit({ favorite_tag: this.data.hobbyTagArr }).then(res => {
      console.log('编辑用户资料', res)

      wx.showToast({ title: '修改成功'})  //提示修改成功

      setTimeout(() => { wx.navigateBack({ delta: 1 }) }, 500)
    })
  },
})