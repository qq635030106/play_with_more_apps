/* 登录方式页 */
import api from '../../../utils/http.js'

Page({

  // 跳转到输入手机号码页
  toPhoneNumberPage: () => {
    // wx.getSetting({
    //   success: res => {
    //     if (!res.authSetting['scope.userInfo']) {
    //       wx.showToast({
    //         title: '请先授权后，在使用手机登录',
    //         icon: 'none',
    //         duration: 1000
    //       })
    //       return false
    //     }
        wx.navigateTo({ url: '/pages/user/phoneNumber/phoneNumber' })
      // }
    // })
  },

  // 用户信息授权
  bindGetUserInfo: (e) => {
    // 用户确认授权,将用户信息存到本地缓存
    let userInfo = e.detail.userInfo
    if (userInfo) {
      wx.setStorage({
        key: 'userInfo',
        data: userInfo,
        success: res => {
          wx.login({
            success: res => {
              if (res.code) {
                // 微信获取用户
                api.getWechat({code: res.code}).then(res => {
                  console.log('微信获取用户', res)
                })
                wx.request({
                  url: `https://api.weixin.qq.com/sns/jscode2session?appid=wx704adecfd70bc581&secret=ee74dd06ad8a023109ed3617672e3644&js_code=${res.code}&grant_type=authorization_code`,
                  success: res => {
                    wx.setStorage({key: 'openid',data: res.data.openid})
                  }
                })
              }
            }
          })
          // wx.showToast({
          //   title: '授权成功',
          //   icon: 'none',
          //   duration: 500
          // })
        }
      })
    }
  },
})