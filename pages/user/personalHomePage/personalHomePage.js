/* 个人主页 */

import api from '../../../utils/http.js'

Page({

  data: {
    userInfo: {}, //我的信息 
    dynamic: [] //我的动态
  }, 

  onShow() {
    let token = wx.getStorageSync('token')
    
    // 我的信息
    this.getMe()

    // 获取我的动态列表
    api.getMyPosts().then(res => {
      console.log('获取我的动态列表', res.data.data)

        let arr = res.data.data.splice(0, 5)

        this.setData({ dynamic: arr })
    })
  },

  // 我的信息
  getMe(){
    api.me().then(res => {
      console.log('我的信息', res.data)

      this.setData({ userInfo: res.data.data })
    })
  },
})