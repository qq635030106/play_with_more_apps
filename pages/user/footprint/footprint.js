/* 浏览足迹页 */
import api from '../../../utils/http.js'
let app = getApp(),  pageStart = 1

Page({
 
  data: {
    footerArr: [],
    isIphoneX: app.globalData.isIphoneX,
    requesting: false,
    end: false,
    emptyShow: false,
    page: pageStart,
    hasTop: false,
    enableBackToTop: false,
    refreshSize: 90,
    bottomSize: 0,
    color: "#1ca0ff",
    lastPage: ''  //总页数
  },

  onLoad() {
    // 浏览足迹
    this.views()
  },

  // 浏览足迹
  views(){
    api.views({page: this.data.page}).then(res => {
      console.log('浏览足迹', res.data.data)

      this.setData({ footerArr: res.data.data, lastPage: res.data.meta.last_page  })
    })
  },

  getList(type, currentPage) {
    this.setData({
      requesting: true
    })

    // 模拟异步获取数据场景
    setTimeout(() => {
      this.setData({
        requesting: false
      })

      if (type === 'refresh') {
        this.setData({
          page: currentPage,
          end: false
        })
        // 浏览足迹
        this.views()

      } else {
        // 请求到最后一页的数据了
        if (currentPage > this.data.lastPage) {
          this.setData({ end: true })
          return false
        }
        api.views({ page: currentPage }).then(res => {
          console.log('浏览足迹', res.data.data)
          // push每一页的数据到数组中
          this.setData({ footerArr: [...this.data.footerArr, ...res.data.data], page: currentPage })
        })
      }

    }, 1000);
  },
  // 下拉刷新
  refresh() {
    this.getList('refresh', pageStart);
  },
  // 上拉加载
  more() {
    this.getList('more', this.data.page + 1);
  }
})

