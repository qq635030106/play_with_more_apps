/* 形象照页 */
import api from '../../../utils/http.js'

Page({

  data: {
    delFlag: false, //勾选
    editText: '编辑',
    uploadText: '上传照片',
    isHighlight: [], //是否选中形象照
    upload_token: '',  //上传所需的token
    userInfo: '',  //我的信息
  },

  onLoad() {
    // 我的信息
    this.getMe()
  },
  // 我的信息
  getMe(){
    api.me().then(res => {
      console.log('我的信息', res.data.data)

      this.setData({ userInfo: res.data.data })
    })
  },
  // 上传
  upload() {
    let { userInfo, isHighlight } = this.data
    if (wx.getStorageSync('photos') === '') wx.setStorage({ key: 'photos', data: [] })
    let photos = wx.getStorageSync('photos')

    if (this.data.uploadText === '上传照片') {
      // 上传数量超过4张给提示
      if (photos.length === 4) {
        wx.showToast({ title: '最多只可以传四张形象照', icon: 'none' })
        return
      }
      // 获取上传token
      api.getUploadToken({ type: 'images' }).then(res => {
        console.log('获取上传token', res)

        this.setData({ upload_token: res.data.data.upload_token })
      })

      // 选择形象照
      wx.chooseImage({
        count: 4 - photos.length, //最多可以选择的图片张数
        sizeType: ['compressed'], //所选的图片的尺寸
        sourceType: ['album', 'camera'],  //选择图片的来源
        success: res => {
          let tempFilePaths = res.tempFilePaths //图片路径
          wx.showLoading({ title: '玩命加载中' }) //加载中
          let fileName = '' //文件名
          let task = []
          // 上传几张形象照就请求几次七牛云

          tempFilePaths.forEach((item, index) => {
            let timestamp = new Date().getTime()
            let fileName = `app/image/${userInfo.id}/${timestamp}`
            task.push(new Promise((resolve, reject) => {
              wx.uploadFile({ //将本地资源上传到服务器
                url: 'http://up-z2.qiniup.com',
                filePath: tempFilePaths[index], //要上传文件资源的路径
                name: 'file', //文件对应的 key，开发者在服务端可以通过这个 key 获取文件的二进制内容
                formData: { //HTTP 请求中其他额外的 form data
                  token: this.data.upload_token,
                  key: fileName,
                },
                success: res => {
                  photos.push(fileName)
                  resolve('成功上传到七牛云')
                }
              })
            }))
          })
          Promise.all(task).then(result => {
            //编辑用户资料
            api.edit({ photos: photos }).then(res => {
              console.log('编辑用户资料', res.data.data)

              this.setData({ userInfo: res.data.data })

              wx.hideLoading()

              wx.setStorage({ key: 'photos', data: photos })
            })
          })
        }
      })
    }
    else {
      // 过滤数组全部为false的情况
      let result = isHighlight.filter(item => item == true)
      if (result.length === 0) {
        wx.showToast({ title: '未选择图片', icon: 'none' })
      } else {
        wx.showModal({
          content: `确定要删除这${result.length}张照片吗`,
          success: res => {
            if (res.confirm) {  //确定删除走这里
              wx.showLoading({ title: '玩命加载中' })
              let photosLen = userInfo.photos.length
              // 获取选中照片的索引
              for (let i = 0; i < photosLen; i++) {
                if (isHighlight[i] === true) {
                  userInfo.photos.splice(i, 1)
                  isHighlight.splice(i, 1)
                  i--
                }
              }

              //删除形象照
              api.edit({ photos: userInfo.photos.length === 0 ? [''] : userInfo.photos }).then(res => {
                console.log('删除形象照后，我的信息', res.data.data)

                this.setData({
                  userInfo: res.data.data,
                  editText: '编辑',
                  uploadText: '上传照片',
                  delFlag: false,

                })

                wx.hideLoading()

                wx.setStorage({ key: 'photos', data: userInfo.photos })
              })
            }
          }
        })
      }
    }
  },
  // 编辑
  edit() {
    let { userInfo, editText, isHighlight} = this.data
    if (userInfo.photos !== null) {
      if (editText === '编辑') {
        // 将所有选中的状态清空
        let result = isHighlight.filter(item => item = false)
        this.setData({
          delFlag: true,
          editText: '取消',
          uploadText: '删除',
          isHighlight: result
        })
      } else {
        this.setData({
          delFlag: false,
          editText: '编辑',
          uploadText: '上传照片'
        })
      }
    } else {
      wx.showToast({title: '请先上传形象照',icon: 'none'})
    }
  },

  // 选中与取消
  idPitch(e) {
    let {id} = e.currentTarget  //当前索引
    let {isHighlight} = this.data
    isHighlight[id] = !isHighlight[id]
    this.setData({isHighlight: isHighlight})
  },

  // 预览
  preview(e) {
    wx.previewImage({
      current: e.target.dataset.src, // 当前显示图片的http链接
      urls: this.data.userInfo.photos // 需要预览的图片http链接列表
    })
  }, 
})