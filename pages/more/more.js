/* 更多页 */

import api from '../../utils/http.js'

Page({

  data: {
    moreList: []
  },

  onLoad() {
    // 获取所有游戏
    this.allGame()
  },

  // 跳转到所点的游戏页
  toGamePage(e) { 
    let { name, id} = e.currentTarget.dataset
    wx.navigateTo({
      url: `/pages/game/game?game_name=${name}&game_id=${id}`
    })
  },

  // 获取所有游戏
  allGame(){
    api.allGame().then(res => {
      console.log('所有游戏', res.data.data)
      
      this.setData({ moreList: res.data.data })
    })
  },
})