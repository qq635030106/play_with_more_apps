/* 附近大神页 */
import api from '../../utils/http'
let app = getApp(),  pageStart = 1

Page({

  data: {
    manito1: [],
    currentPage: 1, //下拉默认页数为第一页
    showLoading: 0, //状态为0代表隐藏loading；用户上拉显示loading，状态为1；请求无数据，状态为2

    isIphoneX: app.globalData.isIphoneX,
    requesting: false,
    end: false,
    emptyShow: false,
    page: pageStart,
    hasTop: false,
    enableBackToTop: false,
    refreshSize: 90,
    bottomSize: 0,
    color: "#1ca0ff",
    lastPage: ''  //总页数
  },

  onLoad() {
    // 附近大神
    this.nearMaster()
  },

  // 附近大神
  nearMaster(){
    api.getNearPlayer({ type: 2, city: wx.getStorageSync('city'), page: this.data.currentPage, page_size: 10}).then(res => {
      console.log('附近大神', res.data.data)
      
      this.setData({ manito1: res.data.data, lastPage: res.data.meta.last_page })
    })
  },

  getList(type, currentPage) {
    this.setData({
      requesting: true
    })

    // 模拟异步获取数据场景
    setTimeout(() => {
      this.setData({
        requesting: false
      })

      if (type === 'refresh') {
        this.setData({
          page: currentPage,
          end: false
        })
        // 附近大神 
        this.nearMaster()

      } else {
        // 请求到最后一页的数据了
        if (currentPage > this.data.lastPage) {
          this.setData({ end: true })
          return false
        }

        // 该游戏下大神列表
        api.getNearPlayer({ type: 2, city: wx.getStorageSync('city'), page: currentPage }).then(res => {
          console.log('该游戏下大神列表', res.data.data)
          // push每一页的数据到数组中
          this.setData({
            manito1: [...this.data.manito1, ...res.data.data],
            page: currentPage
          })

        })

      }

    }, 1000);
  },
  // 下拉刷新
  refresh() {
    this.getList('refresh', pageStart);
  },
  // 上拉加载
  more() {
    this.getList('more', this.data.page + 1);
  }
})