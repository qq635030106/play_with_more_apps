/* 技能资质页 */

import api from '../../utils/http.js'

Page({

  data: { 
    userInfo: {}, //个人数据
    attentionFlag: false,
    attentionText: '关注'
  },

  onLoad(options) {
    // 技能详情 
    this.getSkillDetaill(options.id)
  },

  // 技能详情
  getSkillDetaill(skill_id){
    wx.showNavigationBarLoading()

    api.skillDetail(skill_id).then(res => {
      let userInfo = res.data.data
      console.log('技能详情', userInfo)

      if (userInfo.user.has_follow == 1) {
        this.setData({
          userInfo: userInfo,
          attentionFlag: true,
          attentionText: '已关注' 
        })
      } else {
        this.setData({
          userInfo: userInfo,
          attentionFlag: false,
          attentionText: '关注'
        })
      }
      wx.hideNavigationBarLoading()
    })
  },

  // 是否关注
  isAttention() {
    if (!this.data.attentionFlag) {
      // 关注
      this.attentionFn(true, '已关注')
    } else {
      wx.showModal({
        title: '取消关注',
        content: '确认取消关注当前大神吗?',
        success: res => {
          if (res.confirm) this.attentionFn(false, '关注') // 取消关注
        }
      })
    }
  },

  attentionFn(bool, text) {
    // 关注某个用户
    api.attentionUser(this.data.userInfo.user.id).then(res => {
      console.log('关注某个用户', res)

      if(res.statusCode === 200){
        wx.showToast({ title: res.data.message, icon: 'none' })

        this.setData({ attentionFlag: bool, attentionText: text })
      }else{
        wx.showToast({ title: res.data.message, icon: 'none' })
      }
    })
  },

  // 跳转到聊天页
  toChatPage() {
    let { id, nickname, avatar } = this.data.userInfo.user
    wx.navigateTo({
      url: `/pages/chat/chat?yourId=${id}&yourName=${nickname}&yourAvatar=${avatar}`,
      success: () => {
        // 移除未读消息数
        let userId = wx.getStorageSync('userId')
        wx.removeStorageSync(id + 'and' + userId + '-num')
      }
    })
  },

  // 跳转到提交订单页并传用户数据
  toPlaceOrderPage(e) {
    wx.navigateTo({
      url: '/pages/placeOrder/placeOrder',
      success: res => {
        res.eventChannel.emit('sendUserInfoData', {
          userInfo: this.data.userInfo
        })
      }
    })
  },

  // 预览
  preview(e) {
    wx.previewImage({
      urls: [this.data.userInfo.skill_image] // 需要预览的图片http链接列表
    })
  }
})