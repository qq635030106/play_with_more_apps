// 用户中心页

import api from '../../utils/http.js'

Page({

  data: {
    userInfo: {},
    attentionFlag: false,
    attentionText: '关注',
    dynamic: []
  },

  onLoad(options) {
    // 相关用户id
    let id = options.id

    // 带token，拿相关用户数据
    wx.getStorage({
      key: 'token',
      success: res => {
        // 用户信息
        api.getUser(id).then(res  => {
          console.log('用户信息', res.data.data)

          // 是否已经关注
          if (res.data.data.has_follow === 1) {
            this.setData({attentionFlag: true,attentionText: '已关注'})
          }

          this.setData({ userInfo: res.data.data })
        })
      }
    })

    // 获取某用户动态列表
    api.getPosts(id).then(res => {
      console.log('获取某用户动态列表', res.data.data)

      let arr = res.data.data.splice(0, 5)

      this.setData({ dynamic: arr })
    })
  },

  // 是否关注函数
  attentionFn(bool, text) {
    // 关注某个用户
    api.attentionUser(this.data.userInfo.id, { token: wx.getStorageSync('token') }).then(res => {
      console.log('关注某个用户', res)

      wx.showToast({ title: res.data.message, icon: 'none' })

      this.setData({ attentionFlag: bool, attentionText: text })
    })
  },

  // 是否关注
  isAttention() {
    if (!this.data.attentionFlag) {
      this.attentionFn(true, '已关注')  // 关注
    } else {
      wx.showModal({
        title: '取消关注',
        content: '确认取消关注当前大神吗?',
        success: res => {
          if (res.confirm) this.attentionFn(false, '关注')  // 取消关注
        } 
      })
    }
  },

  // 跳转到聊天页
  toChatPage() {
    let { id, nickname, avatar } = this.data.userInfo
    wx.navigateTo({
      url: `/pages/chat/chat?yourId=${id}&yourName=${nickname}&yourAvatar=${avatar}`,
      success: () => {
        // 移除未读消息数
        let userId = wx.getStorageSync('userId')
        wx.removeStorageSync(id + 'and' + userId + '-num')
      }
    })
  },
})